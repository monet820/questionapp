# questionapp

## API
The api used, auto generated from:
https://opentdb.com/api_config.php
https://opentdb.com/api.php?amount=10&category=31&difficulty=easy&type=boolean

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
